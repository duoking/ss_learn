package com.wry.ss.mapper;

import com.wry.ss.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wry
 * @since 2023-11-08
 */
public interface UserMapper extends BaseMapper<User> {

}
