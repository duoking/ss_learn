package com.wry.ss.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wry
 * @since 2023-11-08
 */
@Controller
@RequestMapping("/ss/user")
public class UserController {

}
