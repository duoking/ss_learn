package com.wry.ss.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {
    @GetMapping("/security/{name}")
    private String m1(@PathVariable String name) {
        return "security," + name;
    }
}
