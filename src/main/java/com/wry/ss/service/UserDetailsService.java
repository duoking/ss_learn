package com.wry.ss.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserDetailsService {

    /**
     * @param username 用户的用户名
     * @return 返回用户信息
     * @throws UsernameNotFoundException 找不到当前用户异常
     */
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

}
